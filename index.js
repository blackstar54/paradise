// Load up the discord.js library
const fs = require('fs'),
    Discord = require("discord.js"),
    axios = require("axios"),
    client = new Discord.Client(),
    config = require("./config.json"),
    request = require("request"),
    striptags = require("striptags"),
    express = require('express'),
    app = express(),
    port = 3000

console.log(`
██████╗  █████╗ ██████╗  █████╗ ██████╗ ██╗███████╗███████╗
██╔══██╗██╔══██╗██╔══██╗██╔══██╗██╔══██╗██║██╔════╝██╔════╝
██████╔╝███████║██████╔╝███████║██║  ██║██║███████╗█████╗  
██╔═══╝ ██╔══██║██╔══██╗██╔══██║██║  ██║██║╚════██║██╔══╝  
██║     ██║  ██║██║  ██║██║  ██║██████╔╝██║███████║███████╗
╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═════╝ ╚═╝╚══════╝╚══════╝
Developed by Lewis (https://youtube.com/Lewisploit)
`)

process.on('uncaughtException', function (err) {
  console.error(err);
  console.log("[PARADISE] Error detected, not closing");
});

const fetchWhitelist = () => require('./whitelist.json'),
        pushWhitelist = id => {
            const Whitelist = fetchWhitelist()
            Whitelist.push(id)

            fs.writeFileSync('./whitelist.json', JSON.stringify(Whitelist), 'utf8')

            return Whitelist
        }

client.on("ready", () => {
  console.log(`[PARADISE] Kickstarted`); 
});

const AIusers = {};
const botSession = {};
let DMBOT = false;

function makeid() {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz123456789";

    for (var i = 0; i < 13; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

//web server

app.get('/whitelist/:id', (req, res) => {
    const {id} = req.params //get ID from request
    const Whitelist = fetchWhitelist()
    request.get("https://discordapp.com/api/v6/users/"+id+"/profile", {
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Accept': '*/*',
            'authorization': config.token,
        },
    }, (err, ress, body) => {
        var jbody = JSON.parse(body);
        name = jbody.user.username
        if (Whitelist.includes(id)) {
            res.send(`${name} is already wjitelisted`)
            return
        }
        pushWhitelist(id)
        console.log("[PARADISE] Added " + name + " to Whitelist")
        res.send(`${name} added to whitelist`) //confirm it works
    });
})

app.get('/whitelist', (req, res) => {
    const whitelist = fetchWhitelist()
    var users = []
    whitelist.forEach(element => {
        request.get("https://discordapp.com/api/v6/users/"+element+"/profile", {
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Accept': '*/*',
                'authorization': config.token,
            },
        }, (err, ress, body) => {
            var jbody = JSON.parse(body);
            name = jbody.user.username
            users.push(name)
            console.log(users)
        });
    });

})

app.get('/status/:state', (req, res) => {
    const {state} = req.params //get ID from request
    client.user.setActivity(state);
    console.log("[PARADISE] Set status to " + state)
    res.send("activity set to " + state)
})

app.get('/toggle', (req, res) => {
    res.charset = 'utf-8'
    DMBOT = !DMBOT
    console.log("[PARADISE] AFK BOT set to " + DMBOT)
    if(DMBOT) { 
        client.user.setStatus('idle')
        client.user.setPresence({
            game: {
                name: 'for DMs',
                type: "watching",
            }
        });
        res.send("AFK Bot is now <u>ON</u>")
    } else {
        client.user.setStatus('online')
        client.user.setActivity("")
        res.send("AFK Bot is now <u>OFF</u>")
    }
})

app.get('/', (req, res) => {
    res.send(fs.readFileSync('./pages/index.html', 'utf8'))
})

app.listen(port, () => console.log(`[PARADISE] Webapp listening on port ${port}!`))

//end webserver

async function getResponse(message) {
    request.post('https://discordapp.com/api/v6/channels/' + message.channel.id + '/typing', {
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Accept': '*/*',
            'authorization': config.token,
        },
    }, (err, res, body) => {
        if(res.statusCode === 204) {
            return console.log("[PARADISE] Sent typing receipt")
        } else {
            console.log(res.statusCode)
        }
    });
    await sleep(2000);

    let gay = message.content
    const msg = encodeURIComponent(gay);
    if (!message.content.replace(/\s/g, '').length) return message.reply("... what do you want?");
    if (message.content.toLowerCase().indexOf("@everyone") > -1) return message.reply("FUCK OFF XD");
    if (message.content.toLowerCase().indexOf("@here") > -1) return message.reply("FUCK OFF XD");
    var clientID = "";
    var sessionID = "";

    if (!AIusers[message.author.id]) {
        AIusers[`${message.author.id}`] = makeid();
        clientID = AIusers[message.author.id];
        request.post('https://miapi.pandorabots.com/talk?botkey=n0M6dW2XZacnOgCWTp0FRYUuMjSfCkJGgobNpgPv9060_72eKnu3Yl-o1v2nFGtSXqfwJBG2Ros~&input=xintro&client_name=' + clientID + '&sessionid=null&channel=6', {
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'Accept': '*/*',
                'Referer': 'https://www.pandorabots.com/mitsuku/',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                'Origin': 'https://www.pandorabots.com'
            },
        }, (err, res, body) => {
            if(body === null) return message.reply("...well this is awkward...")
            var jbody = JSON.parse(body);
            botSession[`${message.author.id}`] = jbody.sessionid;
            sessionID = botSession[message.author.id];
        });
    } else {
        clientID = AIusers[message.author.id];
        sessionID = botSession[message.author.id];
    }

    request.post('https://miapi.pandorabots.com/talk?botkey=n0M6dW2XZacnOgCWTp0FRYUuMjSfCkJGgobNpgPv9060_72eKnu3Yl-o1v2nFGtSXqfwJBG2Ros~&input=' + msg + '&client_name=' + clientID + '&sessionid=' + sessionID + '&channel=6', {
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Accept': '*/*',
            'Referer': 'https://www.pandorabots.com/mitsuku/',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
            'Origin': 'https://www.pandorabots.com'
        },
    }, (err, res, body) => {
        var jbody = JSON.parse(body)
        const myArray = jbody.responses;
        var response = striptags(myArray[0]);

        if(response.indexOf("Error -uuiprod") > -1) {
            AIusers[`${message.author.id}`] = makeid();
            clientID = AIusers[message.author.id];
            request.post('https://miapi.pandorabots.com/talk?botkey=n0M6dW2XZacnOgCWTp0FRYUuMjSfCkJGgobNpgPv9060_72eKnu3Yl-o1v2nFGtSXqfwJBG2Ros~&input=xintro&client_name=' + clientID + '&sessionid=null&channel=6', {
                headers: {
                    'Content-Type': 'application/json; charset=utf-8',
                    'Accept': '*/*',
                    'Referer': 'https://www.pandorabots.com/mitsuku/',
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
                    'Origin': 'https://www.pandorabots.com'
                },
            }, (err, res, body) => {
                var jbody = JSON.parse(body);
                botSession[`${message.author.id}`] = jbody.sessionid;
                sessionID = botSession[message.author.id];
            });
            message.reply("> [PARADISE] Resending request");
            return getResponse(message)
        }
        response = response.replace(/Click here/g, '');
        if (response.indexOf("square-bear") > -1) response = response.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');
        response = response.replace(/  +/g, ' ');
        response = response.trim();
        if (response === "") {
            let embed = new Discord.RichEmbed()
                .setColor([66, 134, 244])
                .setTitle("www.nobodycares.com")
                .setDescription("I'm currently AFK, so heres [Mitsuku](https://www.pandorabots.com/mitsuku).");
            return message.reply({embed});
        }
        let embed = new Discord.RichEmbed()
            .setColor([66, 134, 244])
            .setTitle(response)
            .setDescription("I'm currently AFK, so here's [Mitsuku](https://www.pandorabots.com/mitsuku).");
        message.reply({embed});
        console.log("[PARADISE] " + message.author.tag + ": " + gay + " - " + response);
    });
}

client.on("message", async message => {
  if(message.author.bot) return;

  if(message.channel.type === "dm") {
    if(DMBOT === false) return
    const whitelist = fetchWhitelist()
    if(whitelist.includes(message.author.id) === false) return
    console.log("[PARADISE] Fired AFK Bot")
    getResponse(message)
  }
});

client.login(config.token);