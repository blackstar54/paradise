# Paradise-BOT
A simple yet awesome Discord self-bot: powered by Mitsuku.

## WARNING
Discord HATE self-bot's. You will get banned if you use this! Feel free to move it to a actual Discord bot.

## HOW TO USE
1. Fill in your token in [config.json](config.json)
2. Run [install.bat](install.bat)
3. Run [start.bat](start.bat)
4. Open [localhost:3000](http://localhost:3000) to control the bot

## LICENCE
Make sure to read the [license](LICENSE)!